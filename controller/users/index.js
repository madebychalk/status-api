// Demo data
const defaultUsers = [
  {
    id: 1,
    name: 'Chuck Schuldiner',
    status: 'Playing guitar',
  },
  {
    id: 2,
    name: 'Zbigniew Promiński',
    status: 'Playing drums',
  },
  {
    id: 3,
    name: 'David Ellefson',
    status: 'Playing bass',
  },
];

let users = [
  ...defaultUsers
];

const delay = (data) => new Promise((resolve) => {
  setTimeout(() => {
    resolve(data);
  }, 3000);
})

// Handlers
const getAllUsers = async () => {
  const delay = (users) => new Promise((resolve) => {
    setTimeout(() => {
      resolve(users);
    }, 3000);
  })
  return await delay(users);
}

const getUser = async (req) => {
  const id = Number(req.params.id); // user ID
  return delay(users.find(user => user.id === id));
}

const addUser = async (req, reply) => {
  const id = users.length + 1 // generate new ID
  const newUser = {
    id,
    name: req.body.name,
    status: req.body.status,
  };

  users.push(newUser);
  reply.code(201);
  return delay(newUser);
}

const deleteUsers = async () => {
  users = [...defaultUsers];
  return delay(users);
}

module.exports = {
  getAllUsers,
  getUser,
  addUser,
  deleteUsers,
}
