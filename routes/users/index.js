const blogController = require('../../controller/users');

const routes = [
  {
    method: 'GET',
    url: '/api/users/',
    handler: blogController.getAllUsers
  },
  {
    method: 'GET',
    url: '/api/users/:id',
    handler: blogController.getUser
  },
  {
    method: 'POST',
    url: '/api/users/',
    handler: blogController.addUser
  },
  {
    method: 'DELETE',
    url: '/api/users/',
    handler: blogController.deleteUsers
  },
]
module.exports = routes
