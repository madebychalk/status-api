// Require the framework and instantiate it
const app = require('fastify')({
  logger: false,
});

app.register(require('fastify-cors'), {});

// Declare a route
app.get('/', function (req, reply) {
  reply.send({});
});

// Register routes to handle users
const userRoutes = require('./routes/users');
userRoutes.forEach((route, index) => {
  app.route(route);
});

// Run the server!
app.listen(3000, (err, address) => {
  if (err) {
    app.log.error(err);
    process.exit(1);
  }
  app.log.info(`Server listening on ${address}`);
});
